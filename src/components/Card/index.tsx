import React from "react"
import './style.css'
import { useNavigate } from "react-router-dom";

interface cardProps{
    id: string
    name: string,
    path: string,
    extension: string;
   
}

const Card:React.FC<cardProps> = ({name,path,extension,id}) => {

    let navigate = useNavigate();

    return(
        <>
        <div className="card" onClick={()=>navigate(`/${id}`)}>
            <img src={`${path}.${extension}`} alt=""/>
            <div className="title">{name}</div>
        </div>
        </>
    )
}

export default Card;